﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace RabbitMQDemo
{
    public partial class HelloWorld : Form
    {
        string queueName1 = "hello_queue1";//消费者1
        string queueName2 = "hello_queue2";//消费者2
        Action<string> SetText;
        /// <summary>
        /// 单线程实例
        /// </summary>
        private static readonly HelloWorld _helloWorld;
        static HelloWorld()
        {
            _helloWorld = new HelloWorld();
        }
        /// <summary>
        /// 单例模式
        /// </summary>
        public static HelloWorld SingleForm
        { get { return _helloWorld; } }
        private HelloWorld()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
            ReseiveMsg(queueName1);
            ReseiveMsg(queueName2);
            SetText += OnSetText;
        }

        private void btnSendMsg_Click(object sender, EventArgs e)
        {
            SendMsg();
        }
        /// <summary>
        /// 发送消息
        /// </summary>
        private void SendMsg()
        {
            string message = txtPublisher.Text;
            if (message.Trim().Length <= 0)
            {
                MessageBox.Show("请输入要发送的消息");
            }
            string queueName = cbBoxQueues.SelectedValue.ToString();
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: null,
                                     body: body);
            }
        }

        /// <summary>
        /// 接收消息
        /// </summary>
        private void ReseiveMsg(string queueName)
        {
            //string queueName = cbBoxQueues.SelectedText;
            try
            {
                var factory = new ConnectionFactory() { HostName = "localhost" };

                //connection和channel不能使用using，否则会被dispose掉
                var connection = factory.CreateConnection();
                var channel = connection.CreateModel();
                //声明队列 生产者和消费者都需要QueueDeclare
                channel.QueueDeclare(queue: queueName,
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);

                    txtConsumer1.Invoke(SetText, message);
                };
                channel.BasicConsume(queue: queueName,
                                     noAck: true,
                                     consumer: consumer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void OnSetText(string txtContent)
        {
            string queueName = cbBoxQueues.SelectedValue.ToString();
            if (queueName == queueName1)
                txtConsumer1.Text += string.Format("{0}\r\n", txtPublisher.Text);
            if (queueName == queueName2)
                txtConsumer2.Text += string.Format("{0}\r\n", txtPublisher.Text);
        }

        private void HelloWorld_Load(object sender, EventArgs e)
        {
            List<DataSource> lst = new List<DataSource>();
            lst.Add(new DataSource("消费者1", "hello_queue1"));
            lst.Add(new DataSource("消费者2", "hello_queue2"));

            cbBoxQueues.DataSource = lst;
            cbBoxQueues.DisplayMember = "DisplayMember";
            cbBoxQueues.ValueMember = "DisplayValue";
        }

        private class DataSource
        {
            public DataSource(string displayMember,string displayValue)
            {
                DisplayMember = displayMember;
                DisplayValue = displayValue;
            }
            public string DisplayMember { get; set; }
            public string DisplayValue { get; set; }
        }
    }
}

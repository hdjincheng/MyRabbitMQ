﻿namespace RabbitMQDemo
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnRPC = new System.Windows.Forms.Button();
            this.btnTopics = new System.Windows.Forms.Button();
            this.btnRouting = new System.Windows.Forms.Button();
            this.btnPublishSubscribe = new System.Windows.Forms.Button();
            this.btnWorkQueues = new System.Windows.Forms.Button();
            this.btnHelloWorld = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnRPC);
            this.groupBox1.Controls.Add(this.btnTopics);
            this.groupBox1.Controls.Add(this.btnRouting);
            this.groupBox1.Controls.Add(this.btnPublishSubscribe);
            this.groupBox1.Controls.Add(this.btnWorkQueues);
            this.groupBox1.Controls.Add(this.btnHelloWorld);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(33, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(698, 329);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "6种使用模式";
            // 
            // btnRPC
            // 
            this.btnRPC.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRPC.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnRPC.Location = new System.Drawing.Point(421, 247);
            this.btnRPC.Name = "btnRPC";
            this.btnRPC.Size = new System.Drawing.Size(184, 55);
            this.btnRPC.TabIndex = 5;
            this.btnRPC.Text = "RPC";
            this.btnRPC.UseVisualStyleBackColor = true;
            this.btnRPC.Click += new System.EventHandler(this.btnRPC_Click);
            // 
            // btnTopics
            // 
            this.btnTopics.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnTopics.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnTopics.Location = new System.Drawing.Point(77, 247);
            this.btnTopics.Name = "btnTopics";
            this.btnTopics.Size = new System.Drawing.Size(263, 55);
            this.btnTopics.TabIndex = 4;
            this.btnTopics.Text = "Topics";
            this.btnTopics.UseVisualStyleBackColor = true;
            this.btnTopics.Click += new System.EventHandler(this.btnTopics_Click);
            // 
            // btnRouting
            // 
            this.btnRouting.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRouting.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnRouting.Location = new System.Drawing.Point(421, 151);
            this.btnRouting.Name = "btnRouting";
            this.btnRouting.Size = new System.Drawing.Size(184, 55);
            this.btnRouting.TabIndex = 3;
            this.btnRouting.Text = "Routing";
            this.btnRouting.UseVisualStyleBackColor = true;
            this.btnRouting.Click += new System.EventHandler(this.btnRouting_Click);
            // 
            // btnPublishSubscribe
            // 
            this.btnPublishSubscribe.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnPublishSubscribe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnPublishSubscribe.Location = new System.Drawing.Point(77, 151);
            this.btnPublishSubscribe.Name = "btnPublishSubscribe";
            this.btnPublishSubscribe.Size = new System.Drawing.Size(263, 55);
            this.btnPublishSubscribe.TabIndex = 2;
            this.btnPublishSubscribe.Text = "Publish/Subscribe";
            this.btnPublishSubscribe.UseVisualStyleBackColor = true;
            this.btnPublishSubscribe.Click += new System.EventHandler(this.btnPublishSubscribe_Click);
            // 
            // btnWorkQueues
            // 
            this.btnWorkQueues.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnWorkQueues.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnWorkQueues.Location = new System.Drawing.Point(421, 54);
            this.btnWorkQueues.Name = "btnWorkQueues";
            this.btnWorkQueues.Size = new System.Drawing.Size(184, 55);
            this.btnWorkQueues.TabIndex = 1;
            this.btnWorkQueues.Text = "Work queues";
            this.btnWorkQueues.UseVisualStyleBackColor = true;
            this.btnWorkQueues.Click += new System.EventHandler(this.btnWorkQueues_Click);
            // 
            // btnHelloWorld
            // 
            this.btnHelloWorld.Font = new System.Drawing.Font("宋体", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnHelloWorld.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnHelloWorld.Location = new System.Drawing.Point(77, 54);
            this.btnHelloWorld.Name = "btnHelloWorld";
            this.btnHelloWorld.Size = new System.Drawing.Size(263, 55);
            this.btnHelloWorld.TabIndex = 0;
            this.btnHelloWorld.Text = "Hello World";
            this.btnHelloWorld.UseVisualStyleBackColor = true;
            this.btnHelloWorld.Click += new System.EventHandler(this.btnHelloWorld_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(31, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(656, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "参照官网的6种模式-官网:http://www.rabbitmq.com/tutorials/tutorial-one-dotnet.html\r\n";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 468);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnHelloWorld;
        private System.Windows.Forms.Button btnTopics;
        private System.Windows.Forms.Button btnRouting;
        private System.Windows.Forms.Button btnPublishSubscribe;
        private System.Windows.Forms.Button btnWorkQueues;
        private System.Windows.Forms.Button btnRPC;
        private System.Windows.Forms.Label label1;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RabbitMQDemo
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnHelloWorld_Click(object sender, EventArgs e)
        {
            //一个发送者一个消费者
            HelloWorld.SingleForm.Show();
        }

        private void btnWorkQueues_Click(object sender, EventArgs e)
        {
            //一个发送者多个消费者 消息平均分配 队列和消息持久化 消息确认
            //每个消息（任务）只能发给一个消费者
            WorkQueues.SingleForm.Show();
        }

        private void btnPublishSubscribe_Click(object sender, EventArgs e)
        {
            //发布-交换机-绑定-队列-消费者
            //每个消息（任务）可以同时发给多个消费者
            PublishSubscribe.SingleForm.Show();
        }

        private void btnRouting_Click(object sender, EventArgs e)
        {
            //发布-交换机-路由key-绑定-队列-消费者
            Routing.SingleForm.Show();
        }

        private void btnTopics_Click(object sender, EventArgs e)
        {
            //发布-交换机-路由(正则路由)-绑定-队列-消费者
            Topics.SingleForm.Show();
        }

        private void btnRPC_Click(object sender, EventArgs e)
        {
            //客户端-请求-队列-服务器-队列-回复-客户端
            RPC.SingleForm.Show();
        }
    }
}
